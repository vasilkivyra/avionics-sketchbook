cmake_minimum_required(VERSION 3.15)
project(avionics)

set(CMAKE_C_FLAGS -std=c11)

file(GLOB SRC
        "Src/*.c"
        )

file(GLOB INC
        "Inc/*.h"
        )

file(GLOB_RECURSE MIDDLEWARES
        "Middlewares/*.h"
        "Middlewares/*.c"
        )

file(GLOB_RECURSE DRIVERS
        "Drivers/*.h"
        "Drivers/*.c"
        )

include_directories(Src)
include_directories(Inc)
include_directories(Middlewares/Third_Party/FreeRTOS/Source)
include_directories(Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS)
include_directories(Middlewares/Third_Party/FreeRTOS/Source/include)
include_directories(Middlewares/Third_Party/FreeRTOS/Source/portable)
include_directories(Middlewares/Third_Party/FreeRTOS/Source/portable/GCC)
include_directories(Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F)
include_directories(Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang)
include_directories(Drivers/BMI08x-Sensor-API)
include_directories(Drivers/CMSIS/Device/ST/STM32F4xx/Include)
include_directories(Drivers/CMSIS/Include)
include_directories(Drivers/STM32F4xx_HAL_Driver/Src)
include_directories(Drivers/STM32F4xx_HAL_Driver/Inc)


add_executable(avionics ${INC} ${SRC} ${MIDDLEWARES} ${DRIVERS} Inc/utilities/common.h)