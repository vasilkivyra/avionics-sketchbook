#ifndef DATA_LOGGING_H
#define DATA_LOGGING_H

// UMSATS 2018-2020
//
// Repository:
//  UMSATS/Avionics-2019
//
// File Description:
//  Header file for the data logging module. This uses the flash memory interface to log data to memory.
//
// History
// 2019-04-09 by Joseph Howarth
// - Created.

#include <string.h> 				// For memcpy
#include <inttypes.h>
#include <math.h>
#include "queue.h"
#include "task.h"
#include "configuration.h"

typedef struct UART_HandleTypeDef UART_HandleTypeDef;
typedef struct Flash Flash;

#define FLASH_DATA_BUFFER_SIZE		FLASH_PAGE_SIZE			//Matches flash memory page size.
#define ACC_TYPE 			0x800000
#define GYRO_TYPE			0x400000
#define PRES_TYPE			0x200000
#define	TEMP_TYPE			0x100000

#define DROGUE_DETECT		0x080000
#define DROGUE_DEPLOY		0x040000

#define MAIN_DETECT			0x020000
#define MAIN_DEPLOY			0x010000

#define LAUNCH_DETECT		0x008000
#define LAND_DETECT			0x004000

#define POWER_FAIL			0x002000
#define	OVERCURRENT_EVENT	0x001000

#define	ACC_DATA_SIZE	6
#define	GYRO_DATA_SIZE	6
#define	PRES_DATA_SIZE	3
#define	TEMP_DATA_SIZE	3
#define ALT_DATA_SIZE	4
#define HEADER_SIZE		3
#define DATA_CONTAINER_SIZE (HEADER_SIZE + ACC_DATA_SIZE + GYRO_DATA_SIZE + \
						 PRES_DATA_SIZE + TEMP_DATA_SIZE + ALT_DATA_SIZE)

typedef enum
{
	BUFFER_A = 0,
	BUFFER_B = 1
} BufferSelection_t;


typedef struct
{
	Flash * flash_handle;
	UART_HandleTypeDef * uart;
	configData_t *flightCompConfig;

	QueueHandle_t IMU_data_queue;	//For holding accelerometer and gyroscope readings.
	QueueHandle_t PRES_data_queue;	//For holding pressure and temp. readings.

	TaskHandle_t *timerTask_h;
	
}LoggingStruct_t;


typedef struct
{
	uint8_t data[DATA_CONTAINER_SIZE];
}measurement_container_t;

//-------------------------------------------------------------------------------------------------------------------------------------------------------------
// Description:
//  This task logs data measurements to the flash memory.
//
//	Should be passed a populated LoggingStruct as the parameter.
//	The flash should be initialized before this task is started.
//
// Returns:
//-------------------------------------------------------------------------------------------------------------------------------------------------------------
void logging_task(void * params);



#endif // DATA_LOGGING_H
